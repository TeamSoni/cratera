# Cratera

Cratera is a games-oriented Lua fork.

The only change between Lua and Cratera is a small 50 or so lines,
allowing you to do:

    t:k.f() --> t.k.f(t)
    t:[k].f() --> t[k].f(t)

This syntax is called "component syntax" or "runtime traits".
Since nowadays most games use components one way or another,
I thought it would be a good idea to add them natively.
Given the size of this changeset, there's no reason not to use
this in your next game, except that it isn't (yet) compatible with
LuaJIT, and does require a custom interpreter.

As an added bonus, you can also do

    t:['f']() --> t['f'](t)

Which is just t:f() but with an expression instead of an identifier.  
There are no significant use-cases for this, however.
