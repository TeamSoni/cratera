-- Cratera-specific tests. Run Lua test suite separately.

local t = setmetatable({}, { __tostring=function()return"t"end})
local F = {}
local T = {}
t.t = t
t.tt = t
t[T] = t
t.f = print
t.ff = print
t[F] = print
local _f="f"
local _t="t"

print("------ t:[k]()")
t:f(1) -- plain old lua
t:[_f](2) -- simple string key in register
t:[string.char(string.byte("f"))](3,32,33) -- string key from function
t:["f".."f"](4) -- string key from concatenation
t:["f"..string.sub("afun",2,2)](5,52,53) -- concatenation with function result
t:[(string.sub("afun",2,2))](6,62,63) -- function result in parentheses
t:[(function()return"f"end)()](7) -- closure in key
-- be careful with the ambiguous function call!!!
;(function()return t end)():[(function()return"f"end)()](8) -- closure in object and in key
t:[F](9) -- object key

print("------ t:[k].f()")
t:t.f(1) -- string identifier
t:[_t].f(2) -- string key in register
t:[string.char(string.byte("t"))].f(3,32,33) -- string key from function
t:["t".."t"].f(4) -- string key from concatenation
t:["t"..string.sub("atable",2,2)].f(5,52,53) -- concatenation with function result
t:[(string.sub("atable",2,2))].f(6,62,63) -- function result in parentheses
t:[(function()return"t"end)()].f(7) -- closure in key
-- be careful with the ambiguous function call!!!
;(function()return t end)():[(function()return"t"end)()].f(8) -- closure in object and in key
t:[T].f(9) -- object key


entity = {}

inventory = {get=false, set=false, size=false}
inventory.new=function(size)
  local t = {size=function() return size end}
  function t.set(e, i, o)
    if i <= 0 or i > e:[inventory].size() then error() end
    e[inventory][i] = o
  end
  function t.get(e, i)
    if i <= 0 or i > e:[inventory].size() then error() end
    return e[inventory][i]
  end
  return t
end
inventory.of=function(e) -- helper for passing standalone inventories around
  return {get=function(...)return e:[inventory].get(...)end, set=function(...)return e:[inventory].set(...)end, size=function(...)return e:[inventory].size(...)end}
end

entity[inventory] = inventory.new(5)

entity:[inventory].set(1, "Hello World!")

print(entity:[inventory].get(1))

for i=1, entity:[inventory].size() do
  print(i, entity:[inventory].get(i))
end

local myinv = inventory.of(entity)

for i=1, myinv.size() do
  print("wrapped", i, myinv.get(i))
end
